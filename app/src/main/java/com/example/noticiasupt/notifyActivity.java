package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Api.Servicios.ServiciosPetecion;
import com.example.noticiasupt.viewmodels.Login;
import com.example.noticiasupt.viewmodels.Notificaciones;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class notifyActivity extends AppCompatActivity {
   // private String APITOKEN="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifi);

        Button btnEnviar =(Button)findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            EditText IdUsuario =(EditText)findViewById(R.id.IdUsuario);
            EditText Titulo = (EditText)findViewById(R.id.Titulo);
            EditText Descripcion  = (EditText)findViewById(R.id.Descripcion);
            @Override
            public void onClick(View v) {
                ServiciosPetecion service = Api.getApi(notifyActivity.this).create(ServiciosPetecion.class);
                Call<Notificaciones> NotificacionesCall =  service.enviarNot(Titulo.getText().toString(),Descripcion.getText().toString(),IdUsuario.getText().toString());
                NotificacionesCall.enqueue(new Callback<Notificaciones>() {
                    @Override
                    public void onResponse(Call<Notificaciones> call, Response<Notificaciones> response) {
                        Notificaciones peticion = response.body();
                        if(peticion.estado == "true"){
                           // APITOKEN = peticion.token;
                            //guardarPreferencias();
                            Toast.makeText(notifyActivity.this, "Se envio", Toast.LENGTH_LONG).show();

                        }else{
                            Toast.makeText(notifyActivity.this, "No se envio", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Notificaciones> call, Throwable t) {
                        Toast.makeText(notifyActivity.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
