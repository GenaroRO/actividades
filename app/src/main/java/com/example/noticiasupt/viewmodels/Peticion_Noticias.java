package com.example.noticiasupt.viewmodels;

public class Peticion_Noticias {
    public String estado;
    public String Detalle;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return Detalle;
    }

    public void setDetalle(String detalle) {
        Detalle = detalle;
    }
}
