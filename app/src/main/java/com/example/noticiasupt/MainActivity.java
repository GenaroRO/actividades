package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Api.Servicios.ServiciosPetecion;
import com.example.noticiasupt.viewmodels.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button btncrear;
    private Button btniniciar;
    private String APITOKEN="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btncrear= (Button)findViewById(R.id.btncrear);
        btncrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this,registro.class);
                startActivityForResult(intent,0);
            }
        });

        //Boton de iniciar secion
        Button btniniciar=(Button)findViewById(R.id.btniniciar);
        btniniciar.setOnClickListener(new View.OnClickListener() {
            EditText correo =(EditText)findViewById(R.id.correo);
            EditText password = (EditText)findViewById(R.id.password);
            @Override
            public void onClick(View v) {
                ServiciosPetecion service = Api.getApi(MainActivity.this).create(ServiciosPetecion.class);
                Call<Login> loginCall =  service.getLogin(correo.getText().toString(),password.getText().toString());
                loginCall.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            guardarPreferencias();
                            Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, MenuActivity.class));
                        }else{
                            Toast.makeText(MainActivity.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
    public void guardarPreferencias () {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }


}
